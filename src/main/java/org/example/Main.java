package org.example;


import com.example.data.Apartment;
import com.example.manager.AvitoManager;
import com.example.search.SearchRequest;
import com.example.util.ApartmentTypes;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main {
    public static void main(String[] args) {
        Apartment ap1 = new Apartment(0, ApartmentTypes.STUDIO, 0, 5_000_000, 25_00, false, true, 21, 27);
        Apartment ap2 = new Apartment(0, ApartmentTypes.OPEN_PLAN, 0, 20_000_000, 120_00, true, false, 7, 7);
        Apartment ap3 = new Apartment(0, ApartmentTypes.EXACT, 1, 6_500_000, 31_00, false, false, 1, 5);
        Apartment ap4 = new Apartment(0, ApartmentTypes.EXACT, 5, 100_000_000, 150_20, true, false, 6, 8);
        Apartment ap5 = new Apartment(0, ApartmentTypes.EXACT, 2, 8_000_000, 61_80, false, true, 11, 14);

        SearchRequest searchBalconyBigArea = new SearchRequest(
                ApartmentTypes.ALL_ROOM_TYPES, null, null, null, null, 100_00, null, true, false, null, null, false, false, null, null);

        AvitoManager manager = new AvitoManager();
        manager.create(ap1);
        manager.create(ap2);
        manager.create(ap3);
        manager.create(ap4);
        manager.create(ap5);

        manager.search(searchBalconyBigArea);
        manager.removeById(3);
        log.debug("list with removed 3: {}", manager.getAll());
    }
}
